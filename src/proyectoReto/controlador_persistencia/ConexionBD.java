package proyectoReto.controlador_persistencia;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConexionBD {

    // Atributos
    private String url = "";   // Esta es la ruta de la base de datos
    private Connection con = null;   // Para establece la conexion ala base de datos
    private Statement stmt = null;   // estado cx
    private ResultSet rs = null;   // asignación

    // Constructor
    public ConexionBD(){
        url = "jdbc:sqlite:reto.db";
        try {
            con = DriverManager.getConnection(url);
            if (con != null){
                DatabaseMetaData metaData = con.getMetaData();
                System.out.println("La Conexión fue  exitosa... Conectado MetaData: ");
                System.out.println(metaData.getDriverName());
            }
        } catch (SQLException set){
            System.out.println("Conexión fue errónea... e: " + set.getMessage());
        }
    }

    // Métodos - Funciones
    // Retornar un  estado de la conexión
    public Connection getConnection(){
        return con;
    }

    // Cierre de la conexión
    public void  closeConnection(Connection con){
        if (con != null){
            try {
                con.close();
            } catch (SQLException set){
                Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, set);
                System.out.println("no se pudo cerrar la conexión...");

            }
        }
    }

    // CRUD
    public boolean insertetarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia_sql);
        } catch (SQLException | RuntimeException sr){
            System.out.println(sr.getMessage());
            return false;
        }
        return true;
    }

    // Select
    public ResultSet consultarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sentencia_sql);
        } catch (SQLException set){
            System.out.println(set.getMessage());
        } catch (RuntimeException ruti){
            System.out.println(ruti.getMessage());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return rs;
    }

    // Update
    public boolean actualizarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia_sql);
        } catch (SQLException | RuntimeException sr){
            System.out.println("Error al actualizar los datos : " + sr.getMessage());
            return false;
        }
        return true;
    }

    // Delete
    public boolean borrarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia_sql);
        } catch (SQLException | RuntimeException e){
            System.out.println("Error al borrar los datos: " + e.getMessage());
            return false;
        }
        return true;
    }

    // AutoGuardado
    public boolean setAutoCommitBD(boolean b){
        try {
            con.setAutoCommit(b);
        } catch (SQLException set){
            System.out.println("Error del AutoCommit: " + set.getMessage());
            return false;
        }
        return true;
    }

    // Cerrar la conexión
    public void cerrarConexion(){
        closeConnection(con);
    }

    // Confirmar la sentencia o validar cambios => Commit
    public boolean commitBD(){
        try {
            con.commit();
        } catch (SQLException e){
            System.out.println("Error de commit: " + e.getMessage());
            return false;
        }
        return true;
    }

    // Cancelar sentencia
    public boolean rollbackBD(){
        try {
            con.rollback();
        } catch (SQLException e){
            System.out.println("Error de rollback: " + e.getMessage());
            return false;
        }
        return true;
    }
}


