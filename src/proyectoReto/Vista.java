package proyectoReto;

import proyectoReto.modelo_logica.Producto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Vista extends JFrame {
    private JTabbedPane tabbedPane1;
    private JPanel panel;
    private JTextField idReg_textField1;
    private JTextField nombReg_textField2;
    private JTextField temReg_textField3;
    private JTextField valReg_textField4;
    private JTextField precReg_textField5;
    private JButton guardarButton;
    private JPanel panel1;
    private JPanel panel2;
    private JTable table1;
    private JTextField idCAE_textField6;
    private JTextField nombCAE_textField7;
    private JTextField temCAE_textField8;
    private JTextField valCAE_textField9;
    private JTextField precCAE_textField10;
    private JButton consultarButton;
    private JButton eliminarButton;
    private JButton actualizarButton;

    public Vista(){
        setContentPane(panel);
        pack();
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = nombReg_textField2.getText();
                int temperatura = Integer.parseInt(temReg_textField3.getText());
                double valorBase = Double.parseDouble(valReg_textField4.getText());
                double precio = Double.parseDouble(precReg_textField5.getText());

                Producto producto = new Producto(nombre,temperatura,valorBase,precio);
                if (producto.guardarP()){
                    System.out.println("Producto se guardo correctamente");
                }
                else {
                    System.out.println("Error al duardar el producto");
                }

            }
        });
        consultarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[][] filaDatos = new Object[1][5];
                Object[] columNom = {"id","nombre","temperatura","valorBase","precio"};
                DefaultTableModel model= new DefaultTableModel();
                model.setColumnIdentifiers(columNom);
                table1.setModel(model);

                Producto producto = new Producto();
                List<Producto> lista = producto.listaP();
                for (Producto pr: lista){
                    filaDatos[0][0] = pr.getId() ;
                    filaDatos[0][1] = pr.getNombre();
                    filaDatos[0][2] = pr.getTemperatura();
                    filaDatos[0][3] = pr.getValorBase() ;
                    filaDatos[0][4] = pr.getPrecio();
                    model.addRow(filaDatos[0]);

                }
            }
        });
        actualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(idCAE_textField6.getText());
                String nombre = nombCAE_textField7.getText();
                int temperatura =Integer.parseInt(temCAE_textField8.getText());
                double valorBase = Double.parseDouble(valCAE_textField9.getText());
                double precio = Double.parseDouble(precCAE_textField10.getText());

                Producto producto = new Producto(nombre,temperatura,valorBase,precio);
                producto.setId(id);
                if (producto.actualizarP()){
                    System.out.println("Producto se actualizo correctamente ");
                }
                System.out.println("Error al actualzar");
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(idCAE_textField6.getText());
                String nombre = nombCAE_textField7.getText();
                int temperatura =Integer.parseInt(temCAE_textField8.getText());
                double valorBase = Double.parseDouble(valCAE_textField9.getText());
                double precio = Double.parseDouble(precCAE_textField10.getText());

                Producto producto = new Producto(nombre,temperatura,valorBase,precio);
                producto.setId(id);

                if (producto.eliminarP()){
                    System.out.println("se elimino el produto");
                }

                else {
                    System.out.println("Error de eliminado");
                }

            }
        });
    }
}
