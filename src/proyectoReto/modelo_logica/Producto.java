package proyectoReto.modelo_logica;

import proyectoReto.controlador_persistencia.ConexionBD;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Producto {
    //atribute

    private int id, temperatura ;
    private String nombre;
    private double valorBase, precio;

    //contructor
    public Producto() {}

    public Producto(String nombre, int temperatura,  double valorBase, double precio) {
        this.temperatura = temperatura;
        this.nombre = nombre;
        this.valorBase= valorBase;
        this.precio = precio;
    }
//metodos


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    //Crud
    public boolean guardarP(){
        ConexionBD conexionBD = new ConexionBD();
        String sql = "INSERT INTO productos(nombre ,temperatura,valorBase,precio)" +
                "VALUES('"+ nombre + "'," + temperatura +","+ valorBase +","+ precio +");";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.insertetarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return  true;
            }

            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
      }
      //lectura
    public List<Producto> listaP(){
        List<Producto> listaProductos = new ArrayList<Producto>();
        ConexionBD conexionBD = new ConexionBD();

        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexionBD.consultarBD(sql);
            Producto producto;
            while (rs.next()){
                producto = new Producto();
                producto.setId(rs.getInt("id"));
                producto.setNombre(rs.getString("nombre"));
                producto.setTemperatura(rs.getInt("temperatura"));
                producto.setValorBase(rs.getDouble("valorBase"));
                producto.setPrecio(rs.getDouble("precio"));
                listaProductos.add(producto);
            }
        }catch (SQLException set){
            System.out.println("Se produjo un errar en la lista productos" + set.getMessage());
        }finally {
            conexionBD.cerrarConexion();
        }
        return listaProductos;
    }
    //actualizar
    public  boolean actualizarP(){
        ConexionBD conexionBD = new ConexionBD();
        String sql = "UPDATE productos SET nombre = '"+ nombre + "',temperatura =" + temperatura +
                ",valorBase = "+ valorBase +
                ",precio = "+ precio +
                " WHERE id = "+ id + ";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.actualizarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return  false;
        }
    }
    //eleminar
    public  boolean eliminarP(){
        ConexionBD conexionBD =  new ConexionBD();

        String sql = "DELETE FROM productos WHERE id = "+ id +";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.borrarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }
}
