package proyectoReto;

import proyectoReto.controlador_persistencia.ConexionBD;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;

public class Main {

    public static void main(String[] args) {

        ConexionBD obj= new ConexionBD();
        try (Connection cnx = obj.getConnection()){
            System.out.println("Conexion ok");
            ResultSet rs = obj.consultarBD("select * from productos ");
            System.out.println("rs" + rs);
            System.out.println("|id| nombre | temperatura |valorBase|precio");
            while (rs.next()){
                System.out.println("|" + rs.getInt("id") + "|" + rs.getString("nombre")+ "|" +
                        rs.getInt("temperatura") + "|" +
                        rs.getDouble("valorBase")+ "|" +
                        rs.getDouble("precio"));
            }

        }catch (Exception e){
            System.out.println("error");
            System.out.println(e);
        }

        Vista ventana = new Vista();
        ventana.setSize(800, 600);
        ventana.setVisible(true);
        ventana.setTitle("Reto 5 Mision tic");
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

