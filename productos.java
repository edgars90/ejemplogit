package com.proyecto.proyecto_tic.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="productos")
public class productos implements Serializable{

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="idProductos")
private Integer idProductos  ;

@Column(name="nombre")
private String nombre;

@Column(name="descripción")
private String descripción;

@Column(name="código")
private Integer código;

@Column(name="precio")
private double precio;

@Column(name="stock")
private double stock;

//#LLAVE FORANEA 
@ManyToOne
@JoinColumn(name="idCategoria")
private categorias idCategoria ;


//#Constructores

public productos() {
}




public productos(Integer idProductos, String nombre, String descripción, Integer código, double precio, double stock,
        categorias idCategoria) {
    this.idProductos = idProductos;
    this.nombre = nombre;
    this.descripción = descripción;
    this.código = código;
    this.precio = precio;
    this.stock = stock;
    this.idCategoria = idCategoria;
}





//#getter and setter


public Integer getIdProductos() {
    return idProductos;
}


public void setIdProductos(Integer idProductos) {
    this.idProductos = idProductos;
}

public String getNombre() {
    return nombre;
}

public void setNombre(String nombre) {
    this.nombre = nombre;
}

public String getDescripción() {
    return descripción;
}

public void setDescripción(String descripción) {
    this.descripción = descripción;
}

public Integer getCódigo() {
    return código;
}

public void setCódigo(Integer código) {
    this.código = código;
}

public double getPrecio() {
    return precio;
}

public void setPrecio(double precio) {
    this.precio = precio;
}

public double getStock() {
    return stock;
}

public void setStock(double stock) {
    this.stock = stock;
}

public categorias getIdCategoria() {
    return idCategoria;
}

public void setIdCategoria(categorias idCategoria) {
    this.idCategoria = idCategoria;
}



 












  
}
